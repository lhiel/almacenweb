﻿using AlmacenWeb.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Almacen
{
    public partial class frmSalida : Form
    {
        public frmSalida()
        {
            InitializeComponent();
        }

        private void frmSalida_Load(object sender, EventArgs e)
        {
            cargarDetalleSalida();
            cargarEncargado();
            cargarPersonal();
            cargarMaterial();
            cargarSalida();
            cargarProposito();
        }

        public async void cargarDetalleSalida()
        {
            string res = await ListadoDetalleSalidas();
            List<DetalleSalidaRequest> lista = JsonConvert.DeserializeObject<List<DetalleSalidaRequest>>(res);
            dtgSalida.DataSource = lista;
        }
        public async void cargarSalida()
        {
            string res = await ListadoSalida();
            List<SalidaRequest> lista = JsonConvert.DeserializeObject<List<SalidaRequest>>(res);
            cmbSalida.DataSource = lista;
            cmbSalida.ValueMember = "id";
            cmbSalida.DisplayMember = "id";
        }
        public async Task<string> ListadoDetalleSalidas()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleSalida");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async Task<string> ListadoProposito()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/proposito");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async Task<string> ListadoMaterial()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/material");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async void cargarProposito()
        {
            string res = await ListadoProposito();
            List<PropositoRequest> lista = JsonConvert.DeserializeObject<List<PropositoRequest>>(res);
            cmbProposito.DataSource = lista;
            cmbProposito.ValueMember = "id";
            cmbProposito.DisplayMember = "nombre";
        }
        public async void cargarMaterial()
        {
            string res = await ListadoMaterial();
            List<MaterialRequest> lista = JsonConvert.DeserializeObject<List<MaterialRequest>>(res);
            cmbMaterial.DataSource = lista;
            cmbMaterial.ValueMember = "id";
            cmbMaterial.DisplayMember = "nombre";
        }

        public async Task<string> ListadoSalida()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/salida");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public void insertarSalida()
        {
            string url = "http://localhost:2272/api/salida";
            SalidaRequest salida = new SalidaRequest();
            salida.encargado = Convert.ToInt32(cmbEncargado.SelectedValue);
            salida.personal = Convert.ToInt32(cmbPersonal.SelectedValue);
            salida.estado = "activo";
            string resultado = Send<SalidaRequest>(url, salida, "POST");
            MessageBox.Show("los datos fueron Insertados exitosamente!");

            cargarSalida();
            limpiarS();
        }
        public async Task<string> ListadoPersonal()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/personal");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async void cargarEncargado()
        {
            string res = await ListadoPersonal();
            List<PersonalRequest> lista = JsonConvert.DeserializeObject<List<PersonalRequest>>(res);
            cmbEncargado.DataSource = lista;
            cmbEncargado.ValueMember = "id";
            cmbEncargado.DisplayMember = "nombre";
        }
        public async void cargarPersonal()
        {
            string res = await ListadoPersonal();
            List<PersonalRequest> lista = JsonConvert.DeserializeObject<List<PersonalRequest>>(res);
            cmbPersonal.DataSource = lista;
            cmbPersonal.ValueMember = "id";
            cmbPersonal.DisplayMember = "nombre";
        }
        public void insertarDetalleSalida()
        {
            string url = "http://localhost:2272/api/detalleSalida";
            DetalleSalidaRequest detalle = new DetalleSalidaRequest();
            detalle.salida = Convert.ToInt32(cmbSalida.SelectedValue);
            detalle.material = Convert.ToInt32(cmbMaterial.SelectedValue);
            detalle.cantidad = Convert.ToInt32(txtCantidad.Text);
            detalle.proposito = Convert.ToInt32(cmbProposito.SelectedValue);
            detalle.fecha = DateTime.Now.ToString();
            if (txtObservacion.Text == "")
            {
                txtObservacion.Text = "sin observaciones";
            }
            detalle.observaciones = txtObservacion.Text;
            detalle.estado = "activo";
            string resultado = Send<DetalleSalidaRequest>(url, detalle, "POST");
            MessageBox.Show("los datos fueron Insertados exitosamente!");

            cargarDetalleSalida();
            limpiar();
        }
        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;

            }

            return result;
        }
        public void limpiarS()
        {
            gbingreso.Visible = false;
            button5.Visible = true;

        }
        public void limpiar()
        {
            txtCantidad.Text = "";
            txtId.Text = "";
            txtObservacion.Text = "";
            gbContenido.Visible = false;
            editar.Visible = true;
            eliminar.Visible = true;
            insertarBtn.Visible = true;
            btnInsertar.Visible = false;
            btnEditar.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            insertarSalida();
            limpiarS();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void insertarBtn_Click(object sender, EventArgs e)
        {
            btnInsertar.Visible = true;
            gbContenido.Visible = true;
            button3.Visible = true;


        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            insertarDetalleSalida();
            limpiar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            gbingreso.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            gbingreso.Visible = false;

        }
    }
}
