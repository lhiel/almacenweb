﻿namespace Almacen
{
    partial class frmMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SbtnCancelar = new System.Windows.Forms.Button();
            this.txtSeccion = new System.Windows.Forms.TextBox();
            this.btnCategoria = new System.Windows.Forms.Button();
            this.SbtnAceptar = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSeccion = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbCategoria = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.CbtnAceptar = new System.Windows.Forms.Button();
            this.txtCategoria = new System.Windows.Forms.TextBox();
            this.gbSeccion = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSdescripcion = new System.Windows.Forms.TextBox();
            this.btnEditar = new System.Windows.Forms.Button();
            this.gbContenido = new System.Windows.Forms.GroupBox();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.cbSeccion = new System.Windows.Forms.ComboBox();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.txtUnidad = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.dtgMaterial = new System.Windows.Forms.DataGridView();
            this.insertar = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.editar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbCategoria.SuspendLayout();
            this.gbSeccion.SuspendLayout();
            this.gbContenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaterial)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SbtnCancelar
            // 
            this.SbtnCancelar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.SbtnCancelar.Location = new System.Drawing.Point(176, 98);
            this.SbtnCancelar.Name = "SbtnCancelar";
            this.SbtnCancelar.Size = new System.Drawing.Size(75, 23);
            this.SbtnCancelar.TabIndex = 3;
            this.SbtnCancelar.Text = "Cancelar";
            this.SbtnCancelar.UseVisualStyleBackColor = false;
            this.SbtnCancelar.Click += new System.EventHandler(this.SbtnCancelar_Click);
            // 
            // txtSeccion
            // 
            this.txtSeccion.Location = new System.Drawing.Point(95, 24);
            this.txtSeccion.Name = "txtSeccion";
            this.txtSeccion.Size = new System.Drawing.Size(156, 22);
            this.txtSeccion.TabIndex = 0;
            // 
            // btnCategoria
            // 
            this.btnCategoria.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCategoria.Location = new System.Drawing.Point(153, 578);
            this.btnCategoria.Name = "btnCategoria";
            this.btnCategoria.Size = new System.Drawing.Size(120, 39);
            this.btnCategoria.TabIndex = 22;
            this.btnCategoria.Text = "Nueva Categoria";
            this.btnCategoria.UseVisualStyleBackColor = false;
            this.btnCategoria.Click += new System.EventHandler(this.btnCategoria_Click);
            // 
            // SbtnAceptar
            // 
            this.SbtnAceptar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.SbtnAceptar.Location = new System.Drawing.Point(95, 98);
            this.SbtnAceptar.Name = "SbtnAceptar";
            this.SbtnAceptar.Size = new System.Drawing.Size(75, 23);
            this.SbtnAceptar.TabIndex = 1;
            this.SbtnAceptar.Text = "Aceptar";
            this.SbtnAceptar.UseVisualStyleBackColor = false;
            this.SbtnAceptar.Click += new System.EventHandler(this.SbtnAceptar_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(-246, 416);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 39);
            this.button4.TabIndex = 21;
            this.button4.Text = "Eliminar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(-165, 416);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 39);
            this.button2.TabIndex = 20;
            this.button2.Text = "Editar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-84, 416);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 39);
            this.button1.TabIndex = 19;
            this.button1.Text = "Insertar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnSeccion
            // 
            this.btnSeccion.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnSeccion.Location = new System.Drawing.Point(7, 578);
            this.btnSeccion.Name = "btnSeccion";
            this.btnSeccion.Size = new System.Drawing.Size(112, 39);
            this.btnSeccion.TabIndex = 23;
            this.btnSeccion.Text = "Nueva Seccion";
            this.btnSeccion.UseVisualStyleBackColor = false;
            this.btnSeccion.Click += new System.EventHandler(this.btnSeccion_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCancelar.Location = new System.Drawing.Point(100, 272);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 18;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // gbCategoria
            // 
            this.gbCategoria.Controls.Add(this.label10);
            this.gbCategoria.Controls.Add(this.button3);
            this.gbCategoria.Controls.Add(this.CbtnAceptar);
            this.gbCategoria.Controls.Add(this.txtCategoria);
            this.gbCategoria.Location = new System.Drawing.Point(7, 464);
            this.gbCategoria.Name = "gbCategoria";
            this.gbCategoria.Size = new System.Drawing.Size(267, 108);
            this.gbCategoria.TabIndex = 3;
            this.gbCategoria.TabStop = false;
            this.gbCategoria.Text = "Categoria";
            this.gbCategoria.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 15);
            this.label10.TabIndex = 13;
            this.label10.Text = "Nombre:";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button3.Location = new System.Drawing.Point(176, 63);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Cancelar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // CbtnAceptar
            // 
            this.CbtnAceptar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.CbtnAceptar.Location = new System.Drawing.Point(95, 63);
            this.CbtnAceptar.Name = "CbtnAceptar";
            this.CbtnAceptar.Size = new System.Drawing.Size(75, 23);
            this.CbtnAceptar.TabIndex = 1;
            this.CbtnAceptar.Text = "Aceptar";
            this.CbtnAceptar.UseVisualStyleBackColor = false;
            this.CbtnAceptar.Click += new System.EventHandler(this.CbtnAceptar_Click);
            // 
            // txtCategoria
            // 
            this.txtCategoria.Location = new System.Drawing.Point(70, 24);
            this.txtCategoria.Name = "txtCategoria";
            this.txtCategoria.Size = new System.Drawing.Size(181, 22);
            this.txtCategoria.TabIndex = 0;
            // 
            // gbSeccion
            // 
            this.gbSeccion.Controls.Add(this.label8);
            this.gbSeccion.Controls.Add(this.label9);
            this.gbSeccion.Controls.Add(this.txtSdescripcion);
            this.gbSeccion.Controls.Add(this.SbtnCancelar);
            this.gbSeccion.Controls.Add(this.SbtnAceptar);
            this.gbSeccion.Controls.Add(this.txtSeccion);
            this.gbSeccion.Location = new System.Drawing.Point(7, 326);
            this.gbSeccion.Name = "gbSeccion";
            this.gbSeccion.Size = new System.Drawing.Size(267, 132);
            this.gbSeccion.TabIndex = 2;
            this.gbSeccion.TabStop = false;
            this.gbSeccion.Text = "Seccion";
            this.gbSeccion.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Descripcion:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 15);
            this.label9.TabIndex = 11;
            this.label9.Text = "Nombre:";
            // 
            // txtSdescripcion
            // 
            this.txtSdescripcion.Location = new System.Drawing.Point(95, 50);
            this.txtSdescripcion.Multiline = true;
            this.txtSdescripcion.Name = "txtSdescripcion";
            this.txtSdescripcion.Size = new System.Drawing.Size(155, 44);
            this.txtSdescripcion.TabIndex = 4;
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnEditar.Location = new System.Drawing.Point(19, 272);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 17;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Visible = false;
            // 
            // gbContenido
            // 
            this.gbContenido.BackColor = System.Drawing.Color.DarkCyan;
            this.gbContenido.Controls.Add(this.button4);
            this.gbContenido.Controls.Add(this.button2);
            this.gbContenido.Controls.Add(this.btnCategoria);
            this.gbContenido.Controls.Add(this.button1);
            this.gbContenido.Controls.Add(this.btnSeccion);
            this.gbContenido.Controls.Add(this.btnCancelar);
            this.gbContenido.Controls.Add(this.gbCategoria);
            this.gbContenido.Controls.Add(this.gbSeccion);
            this.gbContenido.Controls.Add(this.btnEditar);
            this.gbContenido.Controls.Add(this.btnInsertar);
            this.gbContenido.Controls.Add(this.cbSeccion);
            this.gbContenido.Controls.Add(this.cbCategoria);
            this.gbContenido.Controls.Add(this.label7);
            this.gbContenido.Controls.Add(this.label6);
            this.gbContenido.Controls.Add(this.label5);
            this.gbContenido.Controls.Add(this.label4);
            this.gbContenido.Controls.Add(this.label3);
            this.gbContenido.Controls.Add(this.label2);
            this.gbContenido.Controls.Add(this.label1);
            this.gbContenido.Controls.Add(this.txtCantidad);
            this.gbContenido.Controls.Add(this.txtUnidad);
            this.gbContenido.Controls.Add(this.txtNombre);
            this.gbContenido.Controls.Add(this.txtCodigo);
            this.gbContenido.Controls.Add(this.txtId);
            this.gbContenido.Font = new System.Drawing.Font("Microsoft JhengHei", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbContenido.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gbContenido.Location = new System.Drawing.Point(646, 11);
            this.gbContenido.Name = "gbContenido";
            this.gbContenido.Size = new System.Drawing.Size(280, 637);
            this.gbContenido.TabIndex = 34;
            this.gbContenido.TabStop = false;
            this.gbContenido.Visible = false;
            // 
            // btnInsertar
            // 
            this.btnInsertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnInsertar.Location = new System.Drawing.Point(181, 272);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(75, 23);
            this.btnInsertar.TabIndex = 16;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Visible = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // cbSeccion
            // 
            this.cbSeccion.FormattingEnabled = true;
            this.cbSeccion.Location = new System.Drawing.Point(135, 233);
            this.cbSeccion.Name = "cbSeccion";
            this.cbSeccion.Size = new System.Drawing.Size(121, 23);
            this.cbSeccion.TabIndex = 15;
            // 
            // cbCategoria
            // 
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(135, 201);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(121, 23);
            this.cbCategoria.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Seccion:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Categoria:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Cantidad:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Unidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Codigo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(34, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(135, 167);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(121, 22);
            this.txtCantidad.TabIndex = 4;
            // 
            // txtUnidad
            // 
            this.txtUnidad.Location = new System.Drawing.Point(135, 130);
            this.txtUnidad.Name = "txtUnidad";
            this.txtUnidad.Size = new System.Drawing.Size(121, 22);
            this.txtUnidad.TabIndex = 3;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(135, 93);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(121, 22);
            this.txtNombre.TabIndex = 2;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(135, 55);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(121, 22);
            this.txtCodigo.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(135, 19);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(121, 22);
            this.txtId.TabIndex = 0;
            // 
            // dtgMaterial
            // 
            this.dtgMaterial.AllowUserToAddRows = false;
            this.dtgMaterial.AllowUserToDeleteRows = false;
            this.dtgMaterial.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dtgMaterial.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgMaterial.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dtgMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgMaterial.Location = new System.Drawing.Point(6, 15);
            this.dtgMaterial.Name = "dtgMaterial";
            this.dtgMaterial.ReadOnly = true;
            this.dtgMaterial.Size = new System.Drawing.Size(618, 557);
            this.dtgMaterial.TabIndex = 0;
            // 
            // insertar
            // 
            this.insertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.insertar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.insertar.Location = new System.Drawing.Point(540, 581);
            this.insertar.Name = "insertar";
            this.insertar.Size = new System.Drawing.Size(75, 39);
            this.insertar.TabIndex = 19;
            this.insertar.Text = "Insertar";
            this.insertar.UseVisualStyleBackColor = false;
            this.insertar.Click += new System.EventHandler(this.insertar_Click);
            // 
            // eliminar
            // 
            this.eliminar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.eliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.eliminar.Location = new System.Drawing.Point(378, 581);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 39);
            this.eliminar.TabIndex = 21;
            this.eliminar.Text = "Eliminar";
            this.eliminar.UseVisualStyleBackColor = false;
            // 
            // editar
            // 
            this.editar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.editar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.editar.Location = new System.Drawing.Point(459, 581);
            this.editar.Name = "editar";
            this.editar.Size = new System.Drawing.Size(75, 39);
            this.editar.TabIndex = 20;
            this.editar.Text = "Editar";
            this.editar.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Controls.Add(this.eliminar);
            this.groupBox1.Controls.Add(this.editar);
            this.groupBox1.Controls.Add(this.insertar);
            this.groupBox1.Controls.Add(this.dtgMaterial);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft JhengHei", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(9, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(631, 637);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Material";
            // 
            // frmMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 661);
            this.Controls.Add(this.gbContenido);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMaterial";
            this.Text = "frmMaterial";
            this.Load += new System.EventHandler(this.frmMaterial_Load);
            this.gbCategoria.ResumeLayout(false);
            this.gbCategoria.PerformLayout();
            this.gbSeccion.ResumeLayout(false);
            this.gbSeccion.PerformLayout();
            this.gbContenido.ResumeLayout(false);
            this.gbContenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMaterial)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SbtnCancelar;
        private System.Windows.Forms.TextBox txtSeccion;
        private System.Windows.Forms.Button btnCategoria;
        private System.Windows.Forms.Button SbtnAceptar;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSeccion;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox gbCategoria;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button CbtnAceptar;
        private System.Windows.Forms.TextBox txtCategoria;
        private System.Windows.Forms.GroupBox gbSeccion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSdescripcion;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.GroupBox gbContenido;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.ComboBox cbSeccion;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.TextBox txtUnidad;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.DataGridView dtgMaterial;
        private System.Windows.Forms.Button insertar;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button editar;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}