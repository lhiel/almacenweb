﻿using AlmacenWeb.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Almacen
{
    public partial class frmIngreso : Form
    {
        public frmIngreso()
        {
            InitializeComponent();
        }

        private void frmIngreso_Load(object sender, EventArgs e)
        {
            cargarIngreso();
            cargarDetalleIngreso();
            cargarEncargado();
            cargarProveedor();
            cargarMaterial();
        }
        public async Task<string> ListadoPersonal()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/personal");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        
        public async Task<string> ListadoDetalleIngresos()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleIngreso");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoIngreso()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/ingreso");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async Task<string> ListadoMaterial()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/material");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async void cargarMaterial()
        {
            string res = await ListadoMaterial();
            List<MaterialRequest> lista = JsonConvert.DeserializeObject<List<MaterialRequest>>(res);
            cmbMaterial.DataSource = lista;
            cmbMaterial.ValueMember = "id";
            cmbMaterial.DisplayMember = "nombre";
            cargarEncargado();
            cargarProveedor();
        }
        public async void cargarIngreso()
        {
            string res = await ListadoIngreso();
            List<IngresoRequest> lista = JsonConvert.DeserializeObject<List<IngresoRequest>>(res);
            cmbIngreso.DataSource = lista;
            cmbIngreso.ValueMember = "id";
            cmbIngreso.DisplayMember = "nombre";
            cargarEncargado();
            cargarProveedor();
        }
        public async void cargarDetalleIngreso()
        {
            string res = await ListadoDetalleIngresos();
            List<DetalleIngresoRequest> lista = JsonConvert.DeserializeObject<List<DetalleIngresoRequest>>(res);
            dtgIngreso.DataSource = lista;
            cargarEncargado();
            cargarProveedor();
        }
        public async void cargarEncargado()
        {
            string res = await ListadoPersonal();
            List<PersonalRequest> lista = JsonConvert.DeserializeObject<List<PersonalRequest>>(res);
            cmbEncargado.DataSource = lista;
            cmbEncargado.ValueMember = "id";
            cmbEncargado.DisplayMember = "nombre";
        }
        public async void cargarProveedor()
        {
            string res = await ListadoPersonal();
            List<PersonalRequest> lista = JsonConvert.DeserializeObject<List<PersonalRequest>>(res);
            cmbProveedor.DataSource = lista;
            cmbProveedor.ValueMember = "id";
            cmbProveedor.DisplayMember = "nombre";
        }
        public void insertarIngreso()
        {
            string url = "http://localhost:2272/api/ingreso";
            IngresoRequest ingreso = new IngresoRequest();
            ingreso.encargado = Convert.ToInt32( cmbEncargado.SelectedValue);
            ingreso.proveedor = Convert.ToInt32( cmbProveedor.SelectedValue);
            ingreso.estado = "activo";
            string resultado = Send<IngresoRequest>(url, ingreso, "POST");
            MessageBox.Show("el ingreso fue Insertado exitosamente!");
            cargarIngreso();
            gbingreso.Visible = false;
            button5.Visible = true;
        }
        public void insertarDetalle()
        {
            string url = "http://localhost:2272/api/detalleIngreso";
            DetalleIngresoRequest detalle = new DetalleIngresoRequest();
            detalle.ingreso = Convert.ToInt32(cmbIngreso.SelectedValue);
            detalle.material = Convert.ToInt32(cmbMaterial.SelectedValue);
            detalle.cantidad = Convert.ToInt32(txtCantidad.Text);
            detalle.fecha = DateTime.Now.ToString();
            detalle.estado = "activo";
            string resultado = Send<DetalleIngresoRequest>(url, detalle, "POST");
            MessageBox.Show("los datos fueron Insertados exitosamente!");
            cargarDetalleIngreso();
            

        }

        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;

            }

            return result;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            insertarIngreso();
            cargarIngreso();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            gbingreso.Visible = false;
            button5.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            gbingreso.Visible = true;
            button5.Visible = false;
            button3.Visible = true;
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            insertarDetalle();
            limpiar();
          
        }
        public void limpiar()
        {
            gbContenido.Visible = false;
            txtId.Text = "";
            txtCantidad.Text = "";
            editar.Visible = true;
            insertar.Visible = true;
            eliminar.Visible = true;
            btnInsertar.Visible = false;
            btnEditar.Visible = false;
        }

        public void activar()
        {
            gbContenido.Visible = true;
            insertar.Visible = false;
            editar.Visible = false;
            btnEditar.Visible = false;
            eliminar.Visible = false;
            btnInsertar.Visible = true;
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
        }

        private void insertar_Click(object sender, EventArgs e)
        {
            activar();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
