﻿namespace Almacen
{
    partial class frmDevolucion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtObservacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.cmbSalida = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgDevolucion = new System.Windows.Forms.DataGridView();
            this.txtId = new System.Windows.Forms.TextBox();
            this.eliminarbtn = new System.Windows.Forms.Button();
            this.editarbtn = new System.Windows.Forms.Button();
            this.gbDevolucion = new System.Windows.Forms.GroupBox();
            this.insertarBtn = new System.Windows.Forms.Button();
            this.gbContenido = new System.Windows.Forms.GroupBox();
            this.cmbMaterial = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDevolucion)).BeginInit();
            this.gbDevolucion.SuspendLayout();
            this.gbContenido.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Observaciones;";
            // 
            // txtObservacion
            // 
            this.txtObservacion.Location = new System.Drawing.Point(26, 210);
            this.txtObservacion.Multiline = true;
            this.txtObservacion.Name = "txtObservacion";
            this.txtObservacion.Size = new System.Drawing.Size(210, 56);
            this.txtObservacion.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(112, 147);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(121, 20);
            this.txtCantidad.TabIndex = 20;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCancelar.Location = new System.Drawing.Point(87, 331);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 18;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnEditar.Location = new System.Drawing.Point(9, 331);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 17;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Visible = false;
            // 
            // btnInsertar
            // 
            this.btnInsertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnInsertar.Location = new System.Drawing.Point(165, 331);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(75, 23);
            this.btnInsertar.TabIndex = 16;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // cmbSalida
            // 
            this.cmbSalida.FormattingEnabled = true;
            this.cmbSalida.Location = new System.Drawing.Point(112, 75);
            this.cmbSalida.Name = "cmbSalida";
            this.cmbSalida.Size = new System.Drawing.Size(121, 21);
            this.cmbSalida.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Salida:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(38, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id:";
            // 
            // dtgDevolucion
            // 
            this.dtgDevolucion.AllowUserToAddRows = false;
            this.dtgDevolucion.AllowUserToDeleteRows = false;
            this.dtgDevolucion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dtgDevolucion.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgDevolucion.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtgDevolucion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDevolucion.Location = new System.Drawing.Point(6, 22);
            this.dtgDevolucion.Name = "dtgDevolucion";
            this.dtgDevolucion.ReadOnly = true;
            this.dtgDevolucion.Size = new System.Drawing.Size(647, 552);
            this.dtgDevolucion.TabIndex = 0;
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(112, 37);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(121, 20);
            this.txtId.TabIndex = 0;
            // 
            // eliminarbtn
            // 
            this.eliminarbtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.eliminarbtn.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.eliminarbtn.Location = new System.Drawing.Point(416, 580);
            this.eliminarbtn.Name = "eliminarbtn";
            this.eliminarbtn.Size = new System.Drawing.Size(75, 39);
            this.eliminarbtn.TabIndex = 21;
            this.eliminarbtn.Text = "Eliminar";
            this.eliminarbtn.UseVisualStyleBackColor = false;
            // 
            // editarbtn
            // 
            this.editarbtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.editarbtn.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.editarbtn.Location = new System.Drawing.Point(497, 580);
            this.editarbtn.Name = "editarbtn";
            this.editarbtn.Size = new System.Drawing.Size(75, 39);
            this.editarbtn.TabIndex = 20;
            this.editarbtn.Text = "Editar";
            this.editarbtn.UseVisualStyleBackColor = false;
            // 
            // gbDevolucion
            // 
            this.gbDevolucion.BackColor = System.Drawing.Color.DarkCyan;
            this.gbDevolucion.Controls.Add(this.eliminarbtn);
            this.gbDevolucion.Controls.Add(this.editarbtn);
            this.gbDevolucion.Controls.Add(this.insertarBtn);
            this.gbDevolucion.Controls.Add(this.dtgDevolucion);
            this.gbDevolucion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbDevolucion.Location = new System.Drawing.Point(7, 12);
            this.gbDevolucion.Name = "gbDevolucion";
            this.gbDevolucion.Size = new System.Drawing.Size(659, 637);
            this.gbDevolucion.TabIndex = 32;
            this.gbDevolucion.TabStop = false;
            this.gbDevolucion.Text = "Devolucion";
            // 
            // insertarBtn
            // 
            this.insertarBtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.insertarBtn.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.insertarBtn.Location = new System.Drawing.Point(578, 580);
            this.insertarBtn.Name = "insertarBtn";
            this.insertarBtn.Size = new System.Drawing.Size(75, 39);
            this.insertarBtn.TabIndex = 19;
            this.insertarBtn.Text = "Insertar";
            this.insertarBtn.UseVisualStyleBackColor = false;
            this.insertarBtn.Click += new System.EventHandler(this.insertarBtn_Click);
            // 
            // gbContenido
            // 
            this.gbContenido.BackColor = System.Drawing.Color.DarkCyan;
            this.gbContenido.Controls.Add(this.cmbMaterial);
            this.gbContenido.Controls.Add(this.label2);
            this.gbContenido.Controls.Add(this.label4);
            this.gbContenido.Controls.Add(this.txtObservacion);
            this.gbContenido.Controls.Add(this.label5);
            this.gbContenido.Controls.Add(this.txtCantidad);
            this.gbContenido.Controls.Add(this.btnCancelar);
            this.gbContenido.Controls.Add(this.btnEditar);
            this.gbContenido.Controls.Add(this.btnInsertar);
            this.gbContenido.Controls.Add(this.cmbSalida);
            this.gbContenido.Controls.Add(this.label6);
            this.gbContenido.Controls.Add(this.label1);
            this.gbContenido.Controls.Add(this.txtId);
            this.gbContenido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbContenido.Location = new System.Drawing.Point(672, 12);
            this.gbContenido.Name = "gbContenido";
            this.gbContenido.Size = new System.Drawing.Size(248, 637);
            this.gbContenido.TabIndex = 33;
            this.gbContenido.TabStop = false;
            this.gbContenido.Visible = false;
            // 
            // cmbMaterial
            // 
            this.cmbMaterial.FormattingEnabled = true;
            this.cmbMaterial.Location = new System.Drawing.Point(112, 109);
            this.cmbMaterial.Name = "cmbMaterial";
            this.cmbMaterial.Size = new System.Drawing.Size(121, 21);
            this.cmbMaterial.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Material:";
            // 
            // frmDevolucion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 661);
            this.Controls.Add(this.gbDevolucion);
            this.Controls.Add(this.gbContenido);
            this.Name = "frmDevolucion";
            this.Text = "frmDevolucion";
            this.Load += new System.EventHandler(this.frmDevolucion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDevolucion)).EndInit();
            this.gbDevolucion.ResumeLayout(false);
            this.gbContenido.ResumeLayout(false);
            this.gbContenido.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtObservacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.ComboBox cmbSalida;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgDevolucion;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button eliminarbtn;
        private System.Windows.Forms.Button editarbtn;
        private System.Windows.Forms.GroupBox gbDevolucion;
        private System.Windows.Forms.Button insertarBtn;
        private System.Windows.Forms.GroupBox gbContenido;
        private System.Windows.Forms.ComboBox cmbMaterial;
        private System.Windows.Forms.Label label2;
    }
}