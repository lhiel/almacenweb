﻿namespace Almacen
{
    partial class frmSalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.eliminar = new System.Windows.Forms.Button();
            this.editar = new System.Windows.Forms.Button();
            this.insertarBtn = new System.Windows.Forms.Button();
            this.dtgSalida = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.cmbProposito = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtObservacion = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.gbingreso = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cmbPersonal = new System.Windows.Forms.ComboBox();
            this.cmbEncargado = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.cmbMaterial = new System.Windows.Forms.ComboBox();
            this.cmbSalida = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbContenido = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSalida)).BeginInit();
            this.gbingreso.SuspendLayout();
            this.gbContenido.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Controls.Add(this.eliminar);
            this.groupBox1.Controls.Add(this.editar);
            this.groupBox1.Controls.Add(this.insertarBtn);
            this.groupBox1.Controls.Add(this.dtgSalida);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(631, 637);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Salidas";
            // 
            // eliminar
            // 
            this.eliminar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.eliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.eliminar.Location = new System.Drawing.Point(387, 473);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 39);
            this.eliminar.TabIndex = 21;
            this.eliminar.Text = "Eliminar";
            this.eliminar.UseVisualStyleBackColor = false;
            // 
            // editar
            // 
            this.editar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.editar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.editar.Location = new System.Drawing.Point(468, 473);
            this.editar.Name = "editar";
            this.editar.Size = new System.Drawing.Size(75, 39);
            this.editar.TabIndex = 20;
            this.editar.Text = "Editar";
            this.editar.UseVisualStyleBackColor = false;
            // 
            // insertarBtn
            // 
            this.insertarBtn.BackColor = System.Drawing.Color.DarkSlateGray;
            this.insertarBtn.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.insertarBtn.Location = new System.Drawing.Point(549, 473);
            this.insertarBtn.Name = "insertarBtn";
            this.insertarBtn.Size = new System.Drawing.Size(75, 39);
            this.insertarBtn.TabIndex = 19;
            this.insertarBtn.Text = "Insertar";
            this.insertarBtn.UseVisualStyleBackColor = false;
            this.insertarBtn.Click += new System.EventHandler(this.insertarBtn_Click);
            // 
            // dtgSalida
            // 
            this.dtgSalida.AllowUserToAddRows = false;
            this.dtgSalida.AllowUserToDeleteRows = false;
            this.dtgSalida.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dtgSalida.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgSalida.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtgSalida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSalida.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dtgSalida.Location = new System.Drawing.Point(6, 15);
            this.dtgSalida.Name = "dtgSalida";
            this.dtgSalida.ReadOnly = true;
            this.dtgSalida.Size = new System.Drawing.Size(618, 452);
            this.dtgSalida.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(136, 278);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(121, 20);
            this.txtCantidad.TabIndex = 20;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCancelar.Location = new System.Drawing.Point(101, 481);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 18;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnEditar.Location = new System.Drawing.Point(20, 481);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 17;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Visible = false;
            // 
            // cmbProposito
            // 
            this.cmbProposito.FormattingEnabled = true;
            this.cmbProposito.Location = new System.Drawing.Point(136, 304);
            this.cmbProposito.Name = "cmbProposito";
            this.cmbProposito.Size = new System.Drawing.Size(121, 21);
            this.cmbProposito.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 312);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Proposito";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 332);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Observaciones;";
            // 
            // txtObservacion
            // 
            this.txtObservacion.Location = new System.Drawing.Point(32, 360);
            this.txtObservacion.Multiline = true;
            this.txtObservacion.Name = "txtObservacion";
            this.txtObservacion.Size = new System.Drawing.Size(237, 56);
            this.txtObservacion.TabIndex = 29;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button5.Location = new System.Drawing.Point(135, 169);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(116, 23);
            this.button5.TabIndex = 28;
            this.button5.Text = "Agregar Salida";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // gbingreso
            // 
            this.gbingreso.Controls.Add(this.button4);
            this.gbingreso.Controls.Add(this.button3);
            this.gbingreso.Controls.Add(this.cmbPersonal);
            this.gbingreso.Controls.Add(this.cmbEncargado);
            this.gbingreso.Controls.Add(this.label2);
            this.gbingreso.Controls.Add(this.label3);
            this.gbingreso.Location = new System.Drawing.Point(7, 15);
            this.gbingreso.Name = "gbingreso";
            this.gbingreso.Size = new System.Drawing.Size(267, 134);
            this.gbingreso.TabIndex = 26;
            this.gbingreso.TabStop = false;
            this.gbingreso.Text = "Numero de Salida";
            this.gbingreso.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button4.Location = new System.Drawing.Point(47, 105);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Cancelar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button3.Location = new System.Drawing.Point(165, 105);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 23);
            this.button3.TabIndex = 27;
            this.button3.Text = "Agregar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cmbPersonal
            // 
            this.cmbPersonal.FormattingEnabled = true;
            this.cmbPersonal.Location = new System.Drawing.Point(123, 66);
            this.cmbPersonal.Name = "cmbPersonal";
            this.cmbPersonal.Size = new System.Drawing.Size(121, 21);
            this.cmbPersonal.TabIndex = 19;
            // 
            // cmbEncargado
            // 
            this.cmbEncargado.FormattingEnabled = true;
            this.cmbEncargado.Location = new System.Drawing.Point(123, 39);
            this.cmbEncargado.Name = "cmbEncargado";
            this.cmbEncargado.Size = new System.Drawing.Size(121, 21);
            this.cmbEncargado.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Personal:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Encargado:";
            // 
            // btnInsertar
            // 
            this.btnInsertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnInsertar.Location = new System.Drawing.Point(182, 481);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(75, 23);
            this.btnInsertar.TabIndex = 16;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Visible = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // cmbMaterial
            // 
            this.cmbMaterial.FormattingEnabled = true;
            this.cmbMaterial.Location = new System.Drawing.Point(136, 251);
            this.cmbMaterial.Name = "cmbMaterial";
            this.cmbMaterial.Size = new System.Drawing.Size(121, 21);
            this.cmbMaterial.TabIndex = 15;
            // 
            // cmbSalida
            // 
            this.cmbSalida.FormattingEnabled = true;
            this.cmbSalida.Location = new System.Drawing.Point(136, 224);
            this.cmbSalida.Name = "cmbSalida";
            this.cmbSalida.Size = new System.Drawing.Size(121, 21);
            this.cmbSalida.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 259);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Material;";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Salida:";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(136, 198);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(121, 20);
            this.txtId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(35, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id:";
            // 
            // gbContenido
            // 
            this.gbContenido.BackColor = System.Drawing.Color.DarkCyan;
            this.gbContenido.Controls.Add(this.cmbProposito);
            this.gbContenido.Controls.Add(this.label8);
            this.gbContenido.Controls.Add(this.label4);
            this.gbContenido.Controls.Add(this.txtObservacion);
            this.gbContenido.Controls.Add(this.button5);
            this.gbContenido.Controls.Add(this.gbingreso);
            this.gbContenido.Controls.Add(this.label5);
            this.gbContenido.Controls.Add(this.txtCantidad);
            this.gbContenido.Controls.Add(this.btnCancelar);
            this.gbContenido.Controls.Add(this.btnEditar);
            this.gbContenido.Controls.Add(this.btnInsertar);
            this.gbContenido.Controls.Add(this.cmbMaterial);
            this.gbContenido.Controls.Add(this.cmbSalida);
            this.gbContenido.Controls.Add(this.label7);
            this.gbContenido.Controls.Add(this.label6);
            this.gbContenido.Controls.Add(this.label1);
            this.gbContenido.Controls.Add(this.txtId);
            this.gbContenido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbContenido.Location = new System.Drawing.Point(646, 12);
            this.gbContenido.Name = "gbContenido";
            this.gbContenido.Size = new System.Drawing.Size(280, 637);
            this.gbContenido.TabIndex = 32;
            this.gbContenido.TabStop = false;
            this.gbContenido.Visible = false;
            // 
            // frmSalida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 661);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbContenido);
            this.Name = "frmSalida";
            this.Text = "frmSalida";
            this.Load += new System.EventHandler(this.frmSalida_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSalida)).EndInit();
            this.gbingreso.ResumeLayout(false);
            this.gbingreso.PerformLayout();
            this.gbContenido.ResumeLayout(false);
            this.gbContenido.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button editar;
        private System.Windows.Forms.Button insertarBtn;
        private System.Windows.Forms.DataGridView dtgSalida;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.ComboBox cmbProposito;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtObservacion;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox gbingreso;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cmbPersonal;
        private System.Windows.Forms.ComboBox cmbEncargado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.ComboBox cmbMaterial;
        private System.Windows.Forms.ComboBox cmbSalida;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbContenido;
    }
}