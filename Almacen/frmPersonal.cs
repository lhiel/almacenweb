﻿using AlmacenWeb.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Almacen
{
    public partial class frmPersonal : Form
    {
        public frmPersonal()
        {
            InitializeComponent();
        }

        private void frmPersonal_Load(object sender, EventArgs e)
        {
            cargarPersonal();
            cargarRol();
        }

        public async Task<string> ListadoPersonal()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/personal");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoRol()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/rol");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async void cargarRol()
        {
            string res = await ListadoRol();
            List<RolRequest> lista = JsonConvert.DeserializeObject<List<RolRequest>>(res);
            cmbRol.DataSource = lista;
            cmbRol.ValueMember = "id";
            cmbRol.DisplayMember = "nombre";
        }

        public async void cargarPersonal()
        {
            string res = await ListadoPersonal();
            List<PersonalRequest> lista = JsonConvert.DeserializeObject<List<PersonalRequest>>(res);
            dtgPersonal.DataSource = lista;
        }

        public void insertarPersonal()
        {
            string url = "http://localhost:2272/api/personal";
            PersonalRequest personal = new PersonalRequest();
            personal.nombre = txtNombre.Text;
            personal.apellido = txtApellido.Text;
            personal.ci = Convert.ToInt32(txtCi.Text);

            personal.celular = Convert.ToInt32(txtCelular.Text);
            personal.rol = Convert.ToInt32(cmbRol.SelectedValue);
            personal.grupo = Convert.ToInt32(txtGrupo.Text);
            personal.estado = "activo";
            string resultado = Send<PersonalRequest>(url, personal, "POST");
            MessageBox.Show("el personal fue insertado exitosamente!");

        }
        public void insertarRol()
        {
            string url = "http://localhost:2272/api/rol";
            RolRequest rol = new RolRequest();
            rol.nombre = txtNombreRol.Text;
            rol.estado = "activo";
            string resultado = Send<RolRequest>(url, rol, "POST");
            MessageBox.Show("el rol fue Insertado exitosamente!");

        }
        public void limpiar()
        {
            txtCi.Text = "";
            txtId.Text = "";
            txtNombre.Text = "";
            txtGrupo.Text = "";
            txtApellido.Text = "";
            txtCelular.Text = "";

        }
        public void activar()
        {
            gbContenido.Visible = true;
            editar.Visible = false;
            insertar.Visible = false;
            eliminar.Visible = false;
            btnInsertar.Visible = true;

        }
        public void desactivar()
        {
            gbContenido.Visible = false;
            editar.Visible = true;
            insertar.Visible = true;
            eliminar.Visible = true;
            btnInsertar.Visible = false;
            btnEditar.Visible = false;
            limpiar();

        }
        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;

            }

            return result;
        }

        private void insertar_Click(object sender, EventArgs e)
        {
            gbContenido.Visible = true;
            activar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            gbRol.Visible = true;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            gbRol.Visible = false;
            txtNombreRol.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            insertarRol();
            gbRol.Visible = false;
            txtNombreRol.Text = "";
            cargarRol();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            insertarPersonal();
            desactivar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            desactivar();
        }
    }
}
