﻿using AlmacenWeb.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Almacen
{
    public partial class frmDevolucion : Form
    {
        public frmDevolucion()
        {
            InitializeComponent();
        }

        private void frmDevolucion_Load(object sender, EventArgs e)
        {
            cargarDevolucion();
            cargarSalida();
            cargarMaterial();
        }
        public async Task<string> ListadoDetalleDevolucion()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleDevolucion");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoMaterial()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/material");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }


        public async Task<string> ListadoSalida()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/salida");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async void cargarSalida()
        {
            string res = await ListadoSalida();
            List<SalidaRequest> lista = JsonConvert.DeserializeObject<List<SalidaRequest>>(res);
            cmbSalida.DataSource = lista;
            cmbSalida.ValueMember = "id";
            cmbSalida.DisplayMember = "id";
        }
        public async void cargarMaterial()
        {
            string res = await ListadoMaterial();
            List<MaterialRequest> lista = JsonConvert.DeserializeObject<List<MaterialRequest>>(res);
            cmbMaterial.DataSource = lista;
            cmbMaterial.ValueMember = "id";
            cmbMaterial.DisplayMember = "nombre";
        }

        public async void cargarDevolucion()
        {
            string res = await ListadoDetalleDevolucion();
            List<DetalleDevolucionRequest> lista = JsonConvert.DeserializeObject<List<DetalleDevolucionRequest>>(res);
            dtgDevolucion.DataSource = lista;
        }
       
        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);
                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                result = e.Message;

            }
            return result;
        }

        public void insertarDetalleDevolucion()
        {
            string url = "http://localhost:2272/api/detalleDevolucion";
            DetalleDevolucionRequest detalle = new DetalleDevolucionRequest();
            detalle.salida = Convert.ToInt32(cmbSalida.SelectedValue);
            detalle.cantidad = Convert.ToInt32(txtCantidad.Text);
            detalle.fecha = DateTime.Now.ToString();
            detalle.material = Convert.ToInt32(cmbMaterial.SelectedValue);
            if (txtObservacion.Text == "")
            {
                txtObservacion.Text = "sin observaciones";
            }
            detalle.observaciones = txtObservacion.Text;
            detalle.estado = "activo";
            MessageBox.Show(detalle.ToString());
            string resultado = Send<DetalleDevolucionRequest>(url, detalle, "POST");
            MessageBox.Show("los datos fueron Insertados exitosamente!");
            cargarDevolucion();
        }


        private void insertarBtn_Click(object sender, EventArgs e)
        {
            activar();
        }
        public void activar()
        {
            btnCancelar.Visible = true;
            gbContenido.Visible = true;
            editarbtn.Visible = false;
            insertarBtn.Visible = false;
            eliminarbtn.Visible = false;
        }
        public void desactivar()
        {
            btnInsertar.Visible = false;
            btnEditar.Visible = false;
            btnCancelar.Visible = false;
            gbContenido.Visible = false;
            editarbtn.Visible = true;
            insertarBtn.Visible = true;
            eliminarbtn.Visible = true;
            txtCantidad.Text = "";
            txtObservacion.Text = "";
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            insertarDetalleDevolucion();
            desactivar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            desactivar();
        }
    }
}
