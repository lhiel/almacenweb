﻿using AlmacenWeb.Models.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;

namespace Almacen
{
    public partial class frmMaterial : Form
    {
        public frmMaterial()
        {
            InitializeComponent();
        }

        private void frmMaterial_Load(object sender, EventArgs e)
        {
            cargarMaterial();
            cargarSeccion();
            cargarCategoria();

        }
        public async void cargarMaterial()
        {
            string res = await ListadoMateriales();
            List<MaterialRequest> lista = JsonConvert.DeserializeObject<List<MaterialRequest>>(res);
            dtgMaterial.DataSource = lista;
        }

        public async void cargarSeccion()
        {
            string res = await ListadoSeccion();
            List<SeccionRequest> lista = JsonConvert.DeserializeObject<List<SeccionRequest>>(res);
            cbSeccion.DataSource = lista;
            cbSeccion.ValueMember = "id";
            cbSeccion.DisplayMember = "nombre";
        }
        public async void cargarCategoria()
        {
            string res = await ListadoCategoria();
            List<SeccionRequest> lista = JsonConvert.DeserializeObject<List<SeccionRequest>>(res);
            cbCategoria.DataSource = lista;
            cbCategoria.ValueMember = "id";
            cbCategoria.DisplayMember = "nombre";
        }
        public async Task<string> ListadoMateriales()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/material");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoSeccion()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/seccion");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoCategoria()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/categoria");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public void insertarMaterial()
        {
            string url = "http://localhost:2272/api/material";
            MaterialRequest material = new MaterialRequest();
            material.codigo = Convert.ToInt32(txtCodigo.Text);
            material.nombre = txtNombre.Text;
            material.cantidad = Convert.ToInt32(txtCantidad.Text);
            material.unidad = txtUnidad.Text;
            material.categoria = Convert.ToInt32(cbCategoria.SelectedValue);
            material.seccion = Convert.ToInt32(cbSeccion.SelectedValue);
            material.estado = "activo";
            string resultado = Send<MaterialRequest>(url, material, "POST");
            MessageBox.Show("los datos fueron Insertados exitosamente!");

        }

        public void actualizarMaterial()
        {
            string url = "http://localhost:2272/api/material";
            MaterialRequest material = new MaterialRequest();
            material.codigo = Convert.ToInt32(txtCodigo.Text);
            material.nombre = txtNombre.Text;
            material.cantidad = Convert.ToInt32(txtCantidad.Text);
            material.unidad = txtUnidad.Text;
            material.categoria = Convert.ToInt32(cbCategoria.SelectedValue);
            material.seccion = Convert.ToInt32(cbSeccion.SelectedValue);
            material.estado = "activo";
            string resultado = Send<MaterialRequest>(url, material, "PUT");
            MessageBox.Show("los datos fueron Insertados exitosamente!");

        }
        

        public void insertarSeccion()
        {
            string url = "http://localhost:2272/api/seccion";
            SeccionRequest seccion = new SeccionRequest();
            seccion.nombre = txtSeccion.Text;
            seccion.descripcion = txtSdescripcion.Text;
            seccion.estado = "activo";
            string resultado = Send<SeccionRequest>(url, seccion, "POST");
            MessageBox.Show("la seccion fue insertada exitosamente!");

        }
        public void insertarCategoria()
        {
            string url = "http://localhost:2272/api/categoria";
            CategoriaRequest categoria = new CategoriaRequest();
            categoria.nombre = txtCategoria.Text;
            categoria.estado = "activo";
            string resultado = Send<CategoriaRequest>(url, categoria, "POST");
            MessageBox.Show("la categoria fue Insertada exitosamente!");

        }

        public string Send<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                //serializamos el objeto
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                //peticion
                WebRequest request = WebRequest.Create(url);
                //headers
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";
                request.Timeout = 10000; //esto es opcional

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;

            }

            return result;
        }
        public void activar()
        {
            gbContenido.Visible = true;

            insertar.Visible = false;
            editar.Visible = false;
            eliminar.Visible = false;
            btnCategoria.Visible = true;
            btnSeccion.Visible = true;
        }
        public void desactivar()
        {
            gbContenido.Visible = false;
            btnInsertar.Visible = false;
            btnEditar.Visible = false;
            eliminar.Visible = true;
            editar.Visible = true;
            insertar.Visible = true;
            btnSeccion.Visible = false;
            btnCategoria.Visible = false;
            limpiar();

        }
        public void limpiar()
        {
            txtId.Text = "";
            txtCodigo.Text = "";
            txtNombre.Text = "";
            txtCantidad.Text = "";
            txtUnidad.Text = "";
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            insertarMaterial();
            desactivar();
            limpiar();
        }

        private void insertar_Click(object sender, EventArgs e)
        {
            btnInsertar.Visible = true;
            activar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            desactivar();
            limpiar();
        }

        private void btnSeccion_Click(object sender, EventArgs e)
        {
            gbSeccion.Visible = true;
        }

        private void SbtnCancelar_Click(object sender, EventArgs e)
        {
            limpiarSec();
        }

        public void limpiarSec()
        {
            txtSeccion.Text = "";
            txtSdescripcion.Text = "";
            txtCategoria.Text = "";
            gbCategoria.Visible = false;
            gbSeccion.Visible = false;
            cargarCategoria();
            cargarSeccion();


        }
        private void btnCategoria_Click(object sender, EventArgs e)
        {
            gbCategoria.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            limpiarSec();
        }

        private void SbtnAceptar_Click(object sender, EventArgs e)
        {
            insertarSeccion();
            limpiarSec();
        }

        private void CbtnAceptar_Click(object sender, EventArgs e)
        {
            insertarCategoria();
            limpiarSec();
        }
    }
}
