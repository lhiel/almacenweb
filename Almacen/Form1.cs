﻿using AlmacenWeb.Models.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Almacen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cargarDetalleSalida();
            cargarSalida2();
            cargarDevolucion();
            cargarDIngreso();
            cargarSalida();
            cargarIngreso();
            cargarMaterial();

        }
        public async void cargarDetalleSalida()
        {
            string res = await ListadoDetalleSalidas();
            List<DetalleSalidaRequest> lista = JsonConvert.DeserializeObject<List<DetalleSalidaRequest>>(res);
            dtgDetalleSalida.DataSource = lista;
        }
        public async void cargarMaterial()
        {
            string res = await ListadoMateriales();
            List<MaterialRequest> lista = JsonConvert.DeserializeObject<List<MaterialRequest>>(res);
            dtgMaterial.DataSource = lista;
        }
        public async Task<string> ListadoMateriales()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/material");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async void cargarSalida()
        {
            string res = await ListadoSalidas();
            List<SalidaRequest> lista = JsonConvert.DeserializeObject<List<SalidaRequest>>(res);
            dtgSalida.DataSource = lista;
        }
        public async void cargarDevolucion()
        {
            string res = await ListadoDetalleDevolucion();
            List<DetalleDevolucionRequest> lista = JsonConvert.DeserializeObject<List<DetalleDevolucionRequest>>(res);
            dtgDetalleDevolucion.DataSource = lista;
        }
        public async void cargarSalida2()
        {
            string res = await ListadoSalidas();
            List<SalidaRequest> lista = JsonConvert.DeserializeObject<List<SalidaRequest>>(res);
            dtgSalida2.DataSource = lista;
        }
        public async void cargarIngreso()
        {
            string res = await ListadoIngresos();
            List<IngresoRequest> lista = JsonConvert.DeserializeObject<List<IngresoRequest>>(res);
            dtgIngreso.DataSource = lista;
        }

       


        public async void cargarDIngreso()
        {
            string res = await ListadoDetalleIngresos();
            List<DetalleIngresoRequest> lista = JsonConvert.DeserializeObject<List<DetalleIngresoRequest>>(res);
            dtgDetalleIngreso.DataSource = lista;
        }
        public async Task<string> ListadoPropositos()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/proposito");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async Task<string> ListadoCategorias()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/categoria");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoDetalleDevolucion()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleDevolucion");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoDetalleIngresos()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleIngreso");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoDetalleSalidas()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/detalleSalida");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoIngresos()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/ingreso");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        public async Task<string> ListadoPersonal()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/personal");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoRoles()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/rol");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoSalidas()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/salida");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoSecciones()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/seccion");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }
        public async Task<string> ListadoUsarios()
        {
            WebRequest request = WebRequest.Create("http://localhost:2272/api/usuario");
            WebResponse response = request.GetResponse();
            StreamReader cadena = new StreamReader(response.GetResponseStream());
            return await cadena.ReadToEndAsync();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmSalida salidas = new frmSalida();
            salidas.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMaterial material = new frmMaterial();
            material.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmIngreso ingreso = new frmIngreso();
            ingreso.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmPersonal personal = new frmPersonal();
            personal.Show();

        }

        private void btnGestion_Click(object sender, EventArgs e)
        {
            frmDevolucion devolucion = new frmDevolucion();
            devolucion.Show();
        }
    }
}
