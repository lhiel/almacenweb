﻿namespace Almacen
{
    partial class frmIngreso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.cmbMaterial = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.gbingreso = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cmbProveedor = new System.Windows.Forms.ComboBox();
            this.cmbEncargado = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbIngreso = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbContenido = new System.Windows.Forms.GroupBox();
            this.dtgIngreso = new System.Windows.Forms.DataGridView();
            this.insertar = new System.Windows.Forms.Button();
            this.editar = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbingreso.SuspendLayout();
            this.gbContenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgIngreso)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 354);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(135, 347);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(121, 20);
            this.txtCantidad.TabIndex = 20;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnCancelar.Location = new System.Drawing.Point(100, 408);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 18;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnEditar.Location = new System.Drawing.Point(19, 408);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 17;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Visible = false;
            // 
            // btnInsertar
            // 
            this.btnInsertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnInsertar.Location = new System.Drawing.Point(181, 408);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(75, 23);
            this.btnInsertar.TabIndex = 16;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.UseVisualStyleBackColor = false;
            this.btnInsertar.Visible = false;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // cmbMaterial
            // 
            this.cmbMaterial.FormattingEnabled = true;
            this.cmbMaterial.Location = new System.Drawing.Point(135, 320);
            this.cmbMaterial.Name = "cmbMaterial";
            this.cmbMaterial.Size = new System.Drawing.Size(121, 21);
            this.cmbMaterial.TabIndex = 15;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button5.Location = new System.Drawing.Point(138, 232);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(116, 23);
            this.button5.TabIndex = 28;
            this.button5.Text = "Agregar Ingreso";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // gbingreso
            // 
            this.gbingreso.Controls.Add(this.button4);
            this.gbingreso.Controls.Add(this.button3);
            this.gbingreso.Controls.Add(this.cmbProveedor);
            this.gbingreso.Controls.Add(this.cmbEncargado);
            this.gbingreso.Controls.Add(this.label2);
            this.gbingreso.Controls.Add(this.label3);
            this.gbingreso.Location = new System.Drawing.Point(7, 15);
            this.gbingreso.Name = "gbingreso";
            this.gbingreso.Size = new System.Drawing.Size(267, 211);
            this.gbingreso.TabIndex = 26;
            this.gbingreso.TabStop = false;
            this.gbingreso.Text = "Mumero de Ingreso";
            this.gbingreso.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button4.Location = new System.Drawing.Point(47, 133);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Cancelar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button3.Location = new System.Drawing.Point(165, 133);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 23);
            this.button3.TabIndex = 27;
            this.button3.Text = "Agregar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cmbProveedor
            // 
            this.cmbProveedor.FormattingEnabled = true;
            this.cmbProveedor.Location = new System.Drawing.Point(123, 94);
            this.cmbProveedor.Name = "cmbProveedor";
            this.cmbProveedor.Size = new System.Drawing.Size(121, 21);
            this.cmbProveedor.TabIndex = 19;
            // 
            // cmbEncargado
            // 
            this.cmbEncargado.FormattingEnabled = true;
            this.cmbEncargado.Location = new System.Drawing.Point(123, 67);
            this.cmbEncargado.Name = "cmbEncargado";
            this.cmbEncargado.Size = new System.Drawing.Size(121, 21);
            this.cmbEncargado.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Proveedor:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Encargado:";
            // 
            // cmbIngreso
            // 
            this.cmbIngreso.FormattingEnabled = true;
            this.cmbIngreso.Location = new System.Drawing.Point(135, 293);
            this.cmbIngreso.Name = "cmbIngreso";
            this.cmbIngreso.Size = new System.Drawing.Size(121, 21);
            this.cmbIngreso.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 328);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Material;";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Ingreso:";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(135, 267);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(121, 20);
            this.txtId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(34, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Id:";
            // 
            // gbContenido
            // 
            this.gbContenido.BackColor = System.Drawing.Color.DarkCyan;
            this.gbContenido.Controls.Add(this.button5);
            this.gbContenido.Controls.Add(this.gbingreso);
            this.gbContenido.Controls.Add(this.label5);
            this.gbContenido.Controls.Add(this.txtCantidad);
            this.gbContenido.Controls.Add(this.btnCancelar);
            this.gbContenido.Controls.Add(this.btnEditar);
            this.gbContenido.Controls.Add(this.btnInsertar);
            this.gbContenido.Controls.Add(this.cmbMaterial);
            this.gbContenido.Controls.Add(this.cmbIngreso);
            this.gbContenido.Controls.Add(this.label7);
            this.gbContenido.Controls.Add(this.label6);
            this.gbContenido.Controls.Add(this.label1);
            this.gbContenido.Controls.Add(this.txtId);
            this.gbContenido.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.gbContenido.Location = new System.Drawing.Point(646, 15);
            this.gbContenido.Name = "gbContenido";
            this.gbContenido.Size = new System.Drawing.Size(280, 637);
            this.gbContenido.TabIndex = 31;
            this.gbContenido.TabStop = false;
            this.gbContenido.Visible = false;
            // 
            // dtgIngreso
            // 
            this.dtgIngreso.AllowUserToAddRows = false;
            this.dtgIngreso.AllowUserToDeleteRows = false;
            this.dtgIngreso.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dtgIngreso.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtgIngreso.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dtgIngreso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgIngreso.Location = new System.Drawing.Point(6, 15);
            this.dtgIngreso.Name = "dtgIngreso";
            this.dtgIngreso.ReadOnly = true;
            this.dtgIngreso.Size = new System.Drawing.Size(618, 559);
            this.dtgIngreso.TabIndex = 0;
            // 
            // insertar
            // 
            this.insertar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.insertar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.insertar.Location = new System.Drawing.Point(551, 590);
            this.insertar.Name = "insertar";
            this.insertar.Size = new System.Drawing.Size(75, 39);
            this.insertar.TabIndex = 19;
            this.insertar.Text = "Insertar";
            this.insertar.UseVisualStyleBackColor = false;
            this.insertar.Click += new System.EventHandler(this.insertar_Click);
            // 
            // editar
            // 
            this.editar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.editar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.editar.Location = new System.Drawing.Point(470, 590);
            this.editar.Name = "editar";
            this.editar.Size = new System.Drawing.Size(75, 39);
            this.editar.TabIndex = 20;
            this.editar.Text = "Editar";
            this.editar.UseVisualStyleBackColor = false;
            // 
            // eliminar
            // 
            this.eliminar.BackColor = System.Drawing.Color.DarkSlateGray;
            this.eliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.eliminar.Location = new System.Drawing.Point(389, 590);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 39);
            this.eliminar.TabIndex = 21;
            this.eliminar.Text = "Eliminar";
            this.eliminar.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Controls.Add(this.eliminar);
            this.groupBox1.Controls.Add(this.editar);
            this.groupBox1.Controls.Add(this.insertar);
            this.groupBox1.Controls.Add(this.dtgIngreso);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(9, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(631, 637);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingresos";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // frmIngreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 661);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbContenido);
            this.Name = "frmIngreso";
            this.Text = "frmIngreso";
            this.Load += new System.EventHandler(this.frmIngreso_Load);
            this.gbingreso.ResumeLayout(false);
            this.gbingreso.PerformLayout();
            this.gbContenido.ResumeLayout(false);
            this.gbContenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgIngreso)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.ComboBox cmbMaterial;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox gbingreso;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cmbProveedor;
        private System.Windows.Forms.ComboBox cmbEncargado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbIngreso;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbContenido;
        private System.Windows.Forms.DataGridView dtgIngreso;
        private System.Windows.Forms.Button insertar;
        private System.Windows.Forms.Button editar;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}