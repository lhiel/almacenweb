//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlmacenWeb
{
    using System;
    using System.Collections.Generic;
    
    public partial class detalleSalida
    {
        public int id { get; set; }
        public Nullable<int> salida { get; set; }
        public Nullable<int> material { get; set; }
        public Nullable<int> cantidad { get; set; }
        public Nullable<int> proposito { get; set; }
        public string fecha { get; set; }
        public string observaciones { get; set; }
        public string estado { get; set; }
    
        public virtual material material1 { get; set; }
        public virtual proposito proposito1 { get; set; }
        public virtual salida salida1 { get; set; }
    }
}
