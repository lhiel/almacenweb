﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class UsuarioRequest
    {
        public Int32 id { get; set; }
        public Int32 personal { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string estado { get; set; }
    }
}