﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class MaterialRequest
    {
        public Int32 id { get; set; }
        public Int32 codigo { get; set; }
        public string nombre { get; set; }
        public Int32 cantidad { get; set; }
        public string unidad { get; set; }
        public Int32 categoria { get; set; }
        public Int32 seccion { get; set; }
        public string estado { get; set; }


    }
}