﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class IngresoRequest
    {
        public Int32 id { get; set; }
        public Int32 encargado { get; set; }
        public Int32 proveedor { get; set; }
        public string estado { get; set; }

    }
}