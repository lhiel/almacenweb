﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class RolRequest
    {
        public Int32 id { get; set; }
        public string nombre { get; set; }
        public string estado { get; set; }

    }
}