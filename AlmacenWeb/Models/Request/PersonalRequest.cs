﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class PersonalRequest
    {
        public Int32 id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }

        public Int32 ci { get; set; }
        public Int32 celular { get; set; }
        public Int32 rol { get; set; }
        public Int32 grupo { get; set; }
        public string estado { get; set; }
    }
}