﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlmacenWeb.Models.Request
{
    public class DetalleIngresoRequest
    {
        public Int32 id { get; set; }
        public Int32 ingreso { get; set; }
        public Int32 material { get; set; }
        public Int32 cantidad { get; set; }
        public string fecha { get; set; }
        public string estado { get; set; }

    }
}