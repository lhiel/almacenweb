﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace AlmacenWeb.Models
{
    public class conexion
    {

        public string CadenaConexion { get; set; }
        public string Consulta { get; set; }
        public DataTable dt = new DataTable();
        private SqlDataAdapter dta;
        //public conexion()
        //{
        //    CadenaConexion = "data source = .;initial catalog = almacen1;security integrated = true";
        //    Consulta = "";

        //}
        public conexion(string consulta)
        {
            CadenaConexion = "data source = .;initial catalog = almacen1; integrated security= true";
            Consulta = consulta;
        }
        //public conexion(string consulta, string cadena)
        //{
        //    CadenaConexion = cadena;
        //    Consulta = consulta;
        //}

        public void Ejecutar()
        {
            if (CadenaConexion == "")
                return;
            else
            {
                SqlConnection cn = new SqlConnection(CadenaConexion);
                if (cn.State == ConnectionState.Closed) cn.Open();
                if (cn.State == ConnectionState.Broken) cn.Open();
              //  cn.Open();
                dta = new SqlDataAdapter(Consulta, cn);
                dta.SelectCommand.CommandType = CommandType.Text;
                dta.Fill(dt);
                cn.Close();
                cn.Dispose();

            }
        }
    }
}