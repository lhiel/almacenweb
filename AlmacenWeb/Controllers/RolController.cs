﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class RolController : ApiController
    {
        public IEnumerable<rol> Get()
        {
            conexion con = new conexion("select * from rol");
            con.Ejecutar();
            rol r;
            List<rol> dato = new List<rol>();
            foreach (DataRow item in con.dt.Rows)
            {
                r = new rol();
                r.id = Convert.ToInt32(item["id"]);
                r.nombre = (item["nombre"]).ToString();
                r.estado = (item["estado"]).ToString();

                dato.Add(r);
            }
            return dato;
        }

        [HttpGet]
        public IEnumerable<rol> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obRol = bd.rol.Where(d => d.id == id).ToList();
                return obRol;
            }
        }
        [HttpGet]
        public IEnumerable<rol> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obRol = bd.rol.Where(d => d.nombre.Contains(dato)).ToList();
                return obRol;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.RolRequest rol)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obRol = new rol();
                obRol.id = rol.id;
                obRol.nombre = rol.nombre;
                obRol.estado = rol.estado;
                bd.rol.Add(obRol);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}
