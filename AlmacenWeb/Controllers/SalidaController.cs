﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class SalidaController : ApiController
    {
        public IEnumerable<salida> Get()
        {
            conexion con = new conexion("select * from salida");
            con.Ejecutar();
            salida sal;
            List<salida> dato = new List<salida>();
            foreach (DataRow item in con.dt.Rows)
            {
                sal = new salida();
                sal.id = Convert.ToInt32(item["id"]);
                sal.encargado = Convert.ToInt32(item["encargado"]);
                sal.personal = Convert.ToInt32(item["personal"]);
                sal.estado = (item["estado"]).ToString();

                dato.Add(sal);
            }
            return dato;
        }

        [HttpGet]
        public IEnumerable<salida> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obSalida = bd.salida.Where(d => d.id == id).ToList();
                return obSalida;
            }
        }
        

        [HttpPost]
        public IHttpActionResult Add(Models.Request.SalidaRequest salida)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obSalida = new salida();
                obSalida.id = salida.id;
                obSalida.encargado = salida.encargado;
                obSalida.personal = salida.personal;
                obSalida.estado = salida.estado;
                bd.salida.Add(obSalida);
                bd.SaveChanges();
            }
            return Ok("");
        }
    }
}
