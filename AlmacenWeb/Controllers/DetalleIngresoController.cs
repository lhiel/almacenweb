﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class DetalleIngresoController : ApiController
    {
        public IEnumerable<detalleIngreso> Get()
        {
           
            //
            conexion con = new conexion("select * from detalleIngreso");
            con.Ejecutar();
            detalleIngreso dIng;
            List<detalleIngreso> dato = new List<detalleIngreso>();
            foreach (DataRow item in con.dt.Rows)
            {
                dIng = new detalleIngreso();
                dIng.id = Convert.ToInt32(item["id"]);
                dIng.ingreso = Convert.ToInt32(item["ingreso"]);
                dIng.material = Convert.ToInt32(item["material"]);
                dIng.cantidad = Convert.ToInt32(item["cantidad"]);
                dIng.fecha = (item["fecha"]).ToString();
                dIng.estado = (item["estado"]).ToString();
                dato.Add(dIng);
            }
            return dato;
            //
        }

        [HttpGet]

        public IEnumerable<detalleIngreso> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obDetalleIngreso = bd.detalleIngreso.Where(d => d.id == id).ToList();
                return obDetalleIngreso;
            }
        }
        
        [HttpPost]
        public IHttpActionResult Add(Models.Request.DetalleIngresoRequest Dingreso)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obDetalleIngreso = new detalleIngreso();
                obDetalleIngreso.id = Dingreso.id;
                obDetalleIngreso.ingreso = Dingreso.ingreso;
                obDetalleIngreso.material = Dingreso.material;
                obDetalleIngreso.cantidad = Dingreso.cantidad;
                obDetalleIngreso.fecha = Dingreso.fecha;
                obDetalleIngreso.estado = Dingreso.estado;
                bd.detalleIngreso.Add(obDetalleIngreso);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}

