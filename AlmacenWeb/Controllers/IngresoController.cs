﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class IngresoController : ApiController
    {
        public IEnumerable<ingreso> Get()
        {
            conexion con = new conexion("select * from ingreso");
            con.Ejecutar();
            ingreso ing;
            List<ingreso> dato = new List<ingreso>();
            foreach (DataRow item in con.dt.Rows)
            {
                ing = new ingreso();
                ing.id = Convert.ToInt32(item["id"]);
                ing.encargado = Convert.ToInt32(item["encargado"]);
                ing.proveedor = Convert.ToInt32(item["proveedor"]);

                ing.estado = (item["estado"]).ToString();
                dato.Add(ing);
            }
            return dato;
        }

        [HttpGet]
        public IEnumerable<ingreso> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obIngreso = bd.ingreso.Where(d => d.id == id).ToList();
                return obIngreso;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.IngresoRequest ingreso)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obIngreso = new ingreso();
                obIngreso.id = ingreso.id;    
                obIngreso.encargado = ingreso.encargado;
                obIngreso.proveedor = ingreso.proveedor;
                obIngreso.estado = ingreso.estado;
                bd.ingreso.Add(obIngreso);
                bd.SaveChanges();
            }
            return Ok("");
        }


    }
}
