﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class SeccionController : ApiController
    {
        public IEnumerable<seccion> Get()
        {
            conexion con = new conexion("select * from seccion");
            con.Ejecutar();
            seccion sec;
            List<seccion> dato = new List<seccion>();
            foreach (DataRow item in con.dt.Rows)
            {
                sec = new seccion();
                sec.id = Convert.ToInt32(item["id"]);
                sec.nombre = (item["nombre"]).ToString();
                sec.descripcion = (item["descripcion"]).ToString();
                sec.estado = (item["estado"]).ToString();

                dato.Add(sec);
            }
            return dato;
        }
        [HttpGet]
        public IEnumerable<seccion> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obSeccion = bd.seccion.Where(d => d.id == id).ToList();
                return obSeccion;
            }
        }
        [HttpGet]
        public IEnumerable<seccion> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obSeccion = bd.seccion.Where(d => d.nombre.Contains(dato)).ToList();
                return obSeccion;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.SeccionRequest seccion)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obSeccion = new seccion();
                obSeccion.id = seccion.id;
                obSeccion.nombre = seccion.nombre;
                obSeccion.descripcion = seccion.descripcion;
                obSeccion.estado = seccion.estado;
                bd.seccion.Add(obSeccion);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}
