﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class PersonalController : ApiController
    {
        public IEnumerable<personal> Get()
        {
            conexion con = new conexion("select * from personal");
            con.Ejecutar();
            personal per;
            List<personal> dato = new List<personal>();
            foreach (DataRow item in con.dt.Rows)
            {
                per = new personal();
                per.id = Convert.ToInt32(item["id"]);
                per.nombre = (item["nombre"]).ToString();
                per.apellido = (item["apellido"]).ToString();
                per.ci = Convert.ToInt32(item["ci"]);
                per.celular = Convert.ToInt32(item["celular"]);
                per.rol = Convert.ToInt32(item["rol"]);
                per.grupo = Convert.ToInt32(item["grupo"]);
                per.estado = (item["estado"]).ToString();
                dato.Add(per);
            }
            return dato;
        }

      
        [HttpGet]

        public IEnumerable<personal> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obPersonal = bd.personal.Where(d => d.nombre.Contains(dato)).ToList();
                return obPersonal;

            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.PersonalRequest personal)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obPersonal = new personal();
                obPersonal.id = personal.id;
                obPersonal.nombre = personal.nombre;
                obPersonal.apellido = personal.apellido;
                obPersonal.ci = personal.ci;
                obPersonal.celular = personal.celular;
                obPersonal.rol = personal.rol;
                obPersonal.grupo = personal.grupo;
                obPersonal.estado = personal.estado;
                bd.personal.Add(obPersonal);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}
