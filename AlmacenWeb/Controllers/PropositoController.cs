﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class PropositoController : ApiController
    {
        public IEnumerable<proposito> Get()
        {
            conexion con = new conexion("select * from proposito");
            con.Ejecutar();
            proposito prop;
            List<proposito> dato = new List<proposito>();
            foreach (DataRow item in con.dt.Rows)
            {
                prop = new proposito();
                prop.id = Convert.ToInt32(item["id"]);
                prop.nombre = (item["nombre"]).ToString();
                prop.estado = (item["estado"]).ToString();

                dato.Add(prop);
            }
            return dato;
        }

        [HttpGet]
        public IEnumerable<proposito> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obProposito = bd.proposito.Where(d => d.id == id).ToList();
                return obProposito;
            }
        }
        [HttpGet]
        public IEnumerable<proposito> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obProposito = bd.proposito.Where(d => d.nombre.Contains(dato)).ToList();
                return obProposito;

            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.PropositoRequest proposito)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obProposito = new proposito();
                obProposito.id = proposito.id;
                obProposito.nombre = proposito.nombre;
                obProposito.estado = proposito.estado;
                bd.proposito.Add(obProposito);
                bd.SaveChanges();
            }
            return Ok("");
        }

    }
}
