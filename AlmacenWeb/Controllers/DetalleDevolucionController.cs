﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class DetalleDevolucionController : ApiController
    {
        [HttpGet]
        public IEnumerable<detalleDevolucion> Get()
        {
            conexion con = new conexion("select * from detalleDevolucion");
            con.Ejecutar();
            detalleDevolucion dev;
            List<detalleDevolucion> dato = new List<detalleDevolucion>();
            foreach (DataRow item in con.dt.Rows)
            {
                dev = new detalleDevolucion();
                dev.id = Convert.ToInt32(item["id"]);
                dev.salida = Convert.ToInt32(item["salida"]);
                dev.material = Convert.ToInt32(item["material"]);
                dev.cantidad = Convert.ToInt32(item["cantidad"]);
                dev.fecha = (item["fecha"]).ToString();
                dev.observaciones = (item["observaciones"]).ToString();
                dev.estado = (item["estado"]).ToString();

                dato.Add(dev);
            }
            return dato;
        }


        //-----

        [HttpGet]

        public IEnumerable<detalleDevolucion> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obDetalleDevolucion= bd.detalleDevolucion.Where(d => d.id == id).ToList();
                return obDetalleDevolucion;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.DetalleDevolucionRequest Ddevolucion)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obDetalleDevolucion= new detalleDevolucion();
                obDetalleDevolucion.id = Ddevolucion.id;
                obDetalleDevolucion.salida = Ddevolucion.salida;
                obDetalleDevolucion.material = Ddevolucion.material;
                obDetalleDevolucion.cantidad = Ddevolucion.cantidad;
                obDetalleDevolucion.fecha = Ddevolucion.fecha;
                obDetalleDevolucion.observaciones = Ddevolucion.observaciones;
                obDetalleDevolucion.estado = Ddevolucion.estado;
                bd.detalleDevolucion.Add(obDetalleDevolucion);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}


