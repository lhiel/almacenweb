﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlmacenWeb.Controllers
{
    public class ProveedorController : Controller
    {
        // GET: Proveedor
        public IEnumerable<personal> Get()
        {
            conexion con = new conexion(" select p.id,p.nombre from personal p join rol r on p.rol = r.id where r.nombre <> 'Encargado'");
            con.Ejecutar();
            personal per;
            List<personal> dato = new List<personal>();
            foreach (DataRow item in con.dt.Rows)
            {
                per = new personal();
                per.id = Convert.ToInt32(item["id"]);
                per.nombre = (item["nombre"]).ToString();
                dato.Add(per);
            }
            return dato;
        }
    }
}