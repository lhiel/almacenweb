﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class MaterialController : ApiController
    {
        public IEnumerable<material> Get()
        {
            conexion con = new conexion("select * from material");
            con.Ejecutar();
            material mat ;
            List<material> dato = new List<material>();
            foreach (DataRow item in con.dt.Rows)
            {
                mat = new material();
                mat.id = Convert.ToInt32(item["id"]);
                mat.codigo = Convert.ToInt32(item["codigo"]);
                mat.nombre = (item["nombre"]).ToString();
                mat.cantidad = Convert.ToInt32(item["cantidad"]);
                mat.unidad = (item["unidad"]).ToString();
                mat.categoria = Convert.ToInt32(item["categoria"]);
                mat.seccion = Convert.ToInt32(item["seccion"]);
                mat.estado = (item["estado"]).ToString();
                dato.Add(mat);
            }
            return dato;
        }

       
        [HttpGet]

        public IEnumerable<material> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obMaterial = bd.material.Where(d => d.nombre.Contains(dato)).ToList();
                return obMaterial;
            }
        }
        [HttpPost]
        public IHttpActionResult Add(Models.Request.MaterialRequest material)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obMaterial = new material();
                obMaterial.id = material.id;
                obMaterial.codigo = material.codigo;
                obMaterial.nombre = material.nombre;
                obMaterial.cantidad = material.cantidad;
                obMaterial.unidad = material.unidad;
                obMaterial.categoria = material.categoria  ;
                obMaterial.seccion = material.seccion;
                obMaterial.estado = material.estado;
                bd.material.Add(obMaterial);
                bd.SaveChanges();
            }
            return Ok("datos insertados correctamente");
        }
    }
}
