﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class CategoriaController : ApiController
    {
        [HttpGet]
        public  IEnumerable<categoria> Get()
        {
            conexion con = new conexion("select * from categoria");
            con.Ejecutar();
            categoria cat ;
            List<categoria> dato = new List<categoria>();

            foreach (DataRow item in con.dt.Rows)
            {
                cat = new categoria();
                cat.id = Convert.ToInt32(item["id"]); 
                cat.nombre = (item["nombre"]).ToString(); 
                cat.estado = (item["estado"]).ToString();
                dato.Add(cat);
            }
            Console.WriteLine(dato);
            return dato;
        }

        [HttpGet]
        public IEnumerable<categoria> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obCategoria = bd.categoria.Where(d => d.id == id).ToList();
                return obCategoria;
            }
        }
        [HttpGet]
        public IEnumerable<categoria> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obCategoria = bd.categoria.Where(d => d.nombre.Contains(dato)).ToList();
                return obCategoria;

            }
        }
        [HttpPost]
        public IHttpActionResult Add(Models.Request.CategoriaRequest categoria)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obCategoria = new categoria();
                obCategoria.id = categoria.id;
                obCategoria.nombre = categoria.nombre;
                obCategoria.estado = categoria.estado;
                bd.categoria.Add(obCategoria);
                bd.SaveChanges();
               
            }


            return Ok("");
        }

    }
}
