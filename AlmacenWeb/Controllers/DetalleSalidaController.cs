﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class DetalleSalidaController : ApiController
    {
        [HttpGet]
        public IEnumerable<detalleSalida> Get()
        {
            conexion con = new conexion("select * from detalleSalida");
            con.Ejecutar();
            detalleSalida dSal;
            List<detalleSalida> dato = new List<detalleSalida>();
            foreach (DataRow item in con.dt.Rows)
            {
                dSal = new detalleSalida();
                dSal.id = Convert.ToInt32(item["id"]);
                dSal.salida = Convert.ToInt32(item["salida"]);
                dSal.material = Convert.ToInt32(item["material"]);
                dSal.cantidad = Convert.ToInt32(item["cantidad"]);
                dSal.proposito = Convert.ToInt32(item["proposito"]);
                dSal.fecha = (item["fecha"]).ToString();
                dSal.observaciones = (item["observaciones"]).ToString();
                dSal.estado = (item["estado"]).ToString();
                dato.Add(dSal);
            }
            return dato;
        }
        [HttpGet]
        public IEnumerable<detalleSalida> Get(int id)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obDsalida = bd.detalleSalida.Where(d => d.id == id).ToList();
                return obDsalida;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.DetalleSalidaRequest Dsalida)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obBDetalleSalida = new detalleSalida();
                obBDetalleSalida.id = Dsalida.id;
                obBDetalleSalida.salida = Dsalida.salida;
                obBDetalleSalida.material = Dsalida.material;
                obBDetalleSalida.cantidad = Dsalida.cantidad;
                obBDetalleSalida.proposito = Dsalida.proposito;
                obBDetalleSalida.fecha = Dsalida.fecha;
                obBDetalleSalida.observaciones = Dsalida.observaciones;
                obBDetalleSalida.estado = Dsalida.estado;
                bd.detalleSalida.Add(obBDetalleSalida);
                bd.SaveChanges();
            }
            return Ok("");
        }
    }
}
