﻿using AlmacenWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlmacenWeb.Controllers
{
    public class UsuarioController : ApiController
    {
        [HttpGet]
        public IEnumerable<usuario> Get()
        {
            conexion con = new conexion("select * from usuario");
            con.Ejecutar();
            usuario usu;
            List<usuario> dato = new List<usuario>();
            foreach (DataRow item in con.dt.Rows)
            {
                usu = new usuario();
                usu.id = Convert.ToInt32(item["id"]);
                usu.personal = Convert.ToInt32(item["personal"]);
                usu.login = (item["login"]).ToString();
                usu.password = (item["password"]).ToString();
                usu.estado = (item["estado"]).ToString();

                dato.Add(usu);
            }
            return dato;
        }

        [HttpGet]
        public IEnumerable<usuario> Get(string dato)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obUsuario = bd.usuario.Where(d => d.login.Contains(dato)).ToList();
                return obUsuario;
            }
        }
        [HttpPost]
        public IHttpActionResult Add(Models.Request.UsuarioRequest usuario)
        {
            using (almacen1Entities bd = new almacen1Entities())
            {
                var obUsuario = new usuario();
                obUsuario.id = usuario.id;
                obUsuario.personal = usuario.personal;
                obUsuario.login = usuario.login;
                obUsuario.password = usuario.password;
                obUsuario.estado = usuario.estado;
                bd.usuario.Add(obUsuario);
                bd.SaveChanges();

            }
            return Ok("");
        }
    }
}
